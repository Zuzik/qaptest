﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace QapXSeven
{
	public class ContactListFragment : ListFragment,AbsListView.IMultiChoiceModeListener
	{
		Zuzik.IStartNewFragment containerActivity;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			RetainInstance = true;
			SetHasOptionsMenu (true);

		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View v = inflater.Inflate (Resource.Layout.fragment_student_list, container, false);

			SetAdapter ();

			ListView lv = v.FindViewById<ListView> (Android.Resource.Id.List);
			lv.ChoiceMode = ChoiceMode.MultipleModal;
			lv.SetMultiChoiceModeListener (this);
			lv.FastScrollEnabled = true;

			return v;
		}

		private void SetAdapter(){
			List<Zuzik.AbsContact> contacts = Zuzik.ContactDatabase.Instance.GetContacts ();
			ContactListAdapter adapter = new ContactListAdapter (Activity, contacts);
			ListAdapter = adapter;
		}

		public override void OnCreateOptionsMenu (IMenu menu, MenuInflater inflater)
		{
			inflater.Inflate (Resource.Menu.menu_student_list, menu);

			SearchManager manager = (SearchManager)Activity.GetSystemService (Context.SearchService);
			SearchView search = (SearchView)menu.FindItem (Resource.Id.search).ActionView;
			search.SetSearchableInfo (manager.GetSearchableInfo (Activity.ComponentName));
			search.QueryTextChange+= (object sender, SearchView.QueryTextChangeEventArgs e) => {
				ProcessQuery(e.NewText);
			};

		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.menu_item_add_new_contact:
				Fragment fragment = ContactChangeFragment.GetInstance ();
				containerActivity.StartNewFragment (fragment);
				return true;
			default:
				return false;
			}
		}

		private void ProcessQuery(string query){
			((ContactListAdapter)ListAdapter).Filter.InvokeFilter (query);
		}

		public override void OnListItemClick (ListView l, View v, int position, long id)
		{
			Zuzik.AbsContact contact = (Zuzik.AbsContact)((ContactListAdapter)ListAdapter).GetItem (position);
			Fragment newFragment = ContactViewFragment.GetInstance (contact.ID);
			containerActivity.StartNewFragment (newFragment);
		}

		public override void OnAttach (Activity activity)
		{
			base.OnAttach (activity);
			containerActivity = (Zuzik.IStartNewFragment)Activity;
		}

		public override void OnDetach ()
		{
			base.OnDetach ();
			containerActivity = null;
		}

		void AbsListView.IMultiChoiceModeListener.OnItemCheckedStateChanged (ActionMode mode, int position, long id, bool @checked){
		}

		bool ActionMode.ICallback.OnActionItemClicked (ActionMode mode, IMenuItem item){
			switch (item.ItemId) {
			case Resource.Id.menu_item_select_all_contact:
				SelectAll (true);
				return true;
			case Resource.Id.menu_item_remove_contact:
				ContactListAdapter adapter = (ContactListAdapter)ListAdapter;
				Zuzik.AbsContactsDataBase rep = Zuzik.ContactDatabase.Instance;
				for (int i = adapter.Count - 1; i >= 0; i--) {
					if (ListView.IsItemChecked (i)) {
						rep.DeleteContact(((Zuzik.AbsContact)adapter.GetItem (i)).ID);
						adapter.RemoveObject (i);
					}
				}
				mode.Finish ();
				adapter.NotifyDataSetChanged ();
				return true;
			default:
				return false;
			}
		}

		private void SelectAll(bool isSelect){
			for (int i = 0; i < ListAdapter.Count; i++) {
				ListView.SetItemChecked (i, isSelect);
			}
		}

		bool ActionMode.ICallback.OnCreateActionMode (ActionMode mode, IMenu menu){
			mode.MenuInflater.Inflate (Resource.Menu.context_menu_student_list, menu);
			return true;
		}

		void ActionMode.ICallback.OnDestroyActionMode (ActionMode mode){
		}

		bool ActionMode.ICallback.OnPrepareActionMode (ActionMode mode, IMenu menu){
			return false;
		}


		private class ContactListAdapter:BaseAdapter<Zuzik.AbsContact>, IFilterable{

			List<Zuzik.AbsContact> contacts;
			Activity context;

			public Filter Filter { get; private set;}

			public ContactListAdapter(Activity context, List<Zuzik.AbsContact> students): base(){
				this.contacts = students;
				this.context = context;
				Filter = new ContactFilter(this);
			}

			public override int Count {
				get {
					return contacts.Count;
				}
			}

			public override View GetView (int position, View convertView, ViewGroup parent)
			{
				if (convertView == null) {
					convertView = context.LayoutInflater.Inflate (Resource.Layout.list_item_student, null);
				}

				Zuzik.AbsContact s = (Zuzik.AbsContact)GetItem (position);

				TextView contact_name = convertView.FindViewById<TextView> (Resource.Id.contact_name);
				contact_name.Text = s.ToString ();

				return convertView;
			}

			public override Zuzik.AbsContact this[int position] {  
				get { return (Zuzik.AbsContact)GetItem(position); }
			}

			public override long GetItemId(int position)
			{
				return position;
			}

			public override Java.Lang.Object GetItem (int position)
			{
				return contacts [position];
			}

			public void RemoveObject(int position){
				contacts.RemoveAt (position);
			}

			private class ContactFilter:Filter{

				private readonly ContactListAdapter _adapter;

				public ContactFilter(ContactListAdapter adapter)
				{
					_adapter = adapter;
				}

				protected override FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
				{
					var returnObj = new FilterResults();
					var results = new List<Zuzik.AbsContact>();
					string con = constraint.ToString ();
					foreach (Zuzik.AbsContact z in Zuzik.ContactDatabase.Instance.GetContacts()) {
						if (z.ToString().ToUpper ().Contains (con.ToUpper ())) {
							results.Add (z);
						}
					}

					returnObj.Values = FromArray(results.Select(r => (Java.Lang.Object) r).ToArray());
					returnObj.Count = results.Count;

					constraint.Dispose ();

					return returnObj;
				}

				protected override void PublishResults(Java.Lang.ICharSequence constraint, FilterResults results)
				{
					using (var values = results.Values)
						_adapter.contacts = values.ToArray<Zuzik.AbsContact> ().ToList ();

					_adapter.NotifyDataSetChanged();

					// Don't do this and see GREF counts rising
					constraint.Dispose();
					results.Dispose();
				}


			}


		}

	}
}

