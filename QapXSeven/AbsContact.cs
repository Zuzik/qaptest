﻿using System;

namespace Zuzik
{
	public abstract class AbsContact: Java.Lang.Object
	{
		public abstract int ID{ get; set; }

		public abstract string FirstName{ get; set; }
		public abstract string SecondName{ get; set; }
		public abstract string ThirdName{ get; set; }

		public abstract DateTime Birthday{ get; set; }

		public abstract string Country{ get; set; }
		public abstract string City{ get; set; }
		public abstract string Street{ get; set; }

		public abstract string Phone{ get; set; }
		public abstract string Email{ get; set; }

		public abstract double Latitude{ get; set; }
		public abstract double Longitude{ get; set; }

	}
}

