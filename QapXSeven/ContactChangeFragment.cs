﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace QapXSeven
{
	public class ContactChangeFragment : Fragment
	{
		private static string EXTRA_CONTACT_ID = "ContactChangeFragment.EXTRA_CONTACT_ID";
		private static string DIALOG_DATE = "dialog_date";
		private static int REQUEST_DATE =0;

		EditText first_name;
		EditText second_name;
		EditText third_name;
		TextView date_of_birth;
		EditText country;
		EditText city;
		EditText street;

		EditText phone;
		EditText email;

		Zuzik.AbsContact contact;

		public static ContactChangeFragment GetInstance(int contactId){
			Bundle args = new Bundle ();
			args.PutInt (EXTRA_CONTACT_ID, contactId);
			ContactChangeFragment fragment = new ContactChangeFragment ();
			fragment.Arguments = args;
			return fragment;
		}

		public static ContactChangeFragment GetInstance(){
			return new ContactChangeFragment ();
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			RetainInstance = true;
			SetHasOptionsMenu (true);

			if (Arguments != null) {
				int contactId = Arguments.GetInt (EXTRA_CONTACT_ID, 0);
				contact = Zuzik.ContactDatabase.Instance.GetContact (contactId);
			} else {
				contact = new Zuzik.Contact ();
			}

		}
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View v = inflater.Inflate (Resource.Layout.fragment_student_change, container, false);
			first_name = (EditText)v.FindViewById (Resource.Id.first_name);
			second_name = (EditText)v.FindViewById (Resource.Id.second_name);
			third_name = (EditText)v.FindViewById (Resource.Id.third_name);
			date_of_birth = (TextView)v.FindViewById (Resource.Id.birthday);
			country = (EditText)v.FindViewById (Resource.Id.country);
			city = (EditText)v.FindViewById (Resource.Id.city);
			street = (EditText)v.FindViewById (Resource.Id.street);
			phone = (EditText)v.FindViewById (Resource.Id.phone);
			email = (EditText)v.FindViewById (Resource.Id.email);

			first_name.Text = contact.FirstName;
			second_name.Text = contact.SecondName;
			third_name.Text = contact.ThirdName;
			date_of_birth.Text = contact.Birthday.ToShortDateString();
			country.Text = contact.Country;
			city.Text = contact.City;
			street.Text = contact.Street;
			phone.Text = contact.Phone;
			email.Text = contact.Email;

			first_name.TextChanged+= (object sender, Android.Text.TextChangedEventArgs e) => {
				contact.FirstName = e.Text.ToString();
			};
			second_name.TextChanged+= (object sender, Android.Text.TextChangedEventArgs e) => {
				contact.SecondName = e.Text.ToString();
			};
			third_name.TextChanged+= (object sender, Android.Text.TextChangedEventArgs e) => {
				contact.ThirdName = e.Text.ToString();
			};
			date_of_birth.Click+= (object sender, EventArgs e) => {
				DatePickerFragment dialog = DatePickerFragment.NewInstance(contact.Birthday);
				dialog.SetTargetFragment(this, REQUEST_DATE);
				dialog.Show(FragmentManager, DIALOG_DATE);

			};
			country.TextChanged+= (object sender, Android.Text.TextChangedEventArgs e) => {
				contact.Country = e.Text.ToString();
			};
			city.TextChanged+= (object sender, Android.Text.TextChangedEventArgs e) => {
				contact.City = e.Text.ToString();
			};
			street.TextChanged+= (object sender, Android.Text.TextChangedEventArgs e) => {
				contact.Street = e.Text.ToString();
			};
			phone.TextChanged+= (object sender, Android.Text.TextChangedEventArgs e) => {
				contact.Phone = e.Text.ToString();
			};
			email.TextChanged+= (object sender, Android.Text.TextChangedEventArgs e) => {
				contact.Email = e.Text.ToString();
			};

			return v;
		}

		public override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			if (resultCode != Result.Ok)
				return;
			if (requestCode == REQUEST_DATE) {
				int year = data.GetIntExtra (DatePickerFragment.EXTRA_YEAR, 0);
				int month = data.GetIntExtra (DatePickerFragment.EXTRA_MONTH, 0);
				int day = data.GetIntExtra (DatePickerFragment.EXTRA_DAY, 0);
				contact.Birthday = new DateTime (year, month, day);
				date_of_birth.Text = contact.Birthday.ToShortDateString();
			}
		}

		public override void OnSaveInstanceState (Bundle outState)
		{
			base.OnSaveInstanceState (outState);
		}

		public override void OnCreateOptionsMenu (IMenu menu, MenuInflater inflater)
		{
			inflater.Inflate (Resource.Menu.menu_student_change, menu);
		}

		private bool CheckInputData(){
			if (first_name.Text == "") {
				Toast.MakeText (Activity, Resource.String.incorrect_first_name, ToastLength.Short).Show ();
				first_name.RequestFocusFromTouch ();
				return false;
			}
			if (second_name.Text == "") {
				Toast.MakeText (Activity, Resource.String.incorrect_second_name, ToastLength.Short).Show ();
				second_name.RequestFocusFromTouch ();
				return false;
			}
			if (third_name.Text == "") {
				Toast.MakeText (Activity, Resource.String.incorrect_third_name, ToastLength.Short).Show ();
				third_name.RequestFocusFromTouch ();
				return false;
			}
			if (country.Text == "") {
				Toast.MakeText (Activity, Resource.String.incorrect_country, ToastLength.Short).Show ();
				country.RequestFocusFromTouch ();
				return false;
			}
			if (city.Text == "") {
				Toast.MakeText (Activity, Resource.String.incorrect_city, ToastLength.Short).Show ();
				city.RequestFocusFromTouch ();
				return false;
			}
			if (street.Text == "") {
				Toast.MakeText (Activity, Resource.String.incorrect_street, ToastLength.Short).Show ();
				street.RequestFocusFromTouch ();
				return false;
			}

			return true;
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.menu_item_save_contact:
				if (CheckInputData ()) {
					Zuzik.ContactDatabase.Instance.SaveContact(contact);
					Activity.OnBackPressed ();
				}
				return true;
			default:
				return false;
			}
		}

	}
}

