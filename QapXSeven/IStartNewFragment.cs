﻿using System;
using Android.App;

namespace Zuzik
{
	public interface IStartNewFragment
	{
		void StartNewFragment(Fragment fragment);
	}
}

