﻿using System;
using SQLite;

namespace Zuzik
{
	[Table("Contacts")]
	public class Contact: Zuzik.AbsContact
	{
		[PrimaryKey, AutoIncrement]
		public override int ID { get; set; }

		[MaxLength(256)]
		public override string FirstName { get; set; }
		[MaxLength(256)]
		public override string SecondName { get; set; }
		[MaxLength(256)]
		public override string ThirdName { get; set; }

		public override DateTime Birthday { get; set; }

		[MaxLength(256)]
		public override string Country { get; set; }
		[MaxLength(256)]
		public override string City { get; set; }
		[MaxLength(256)]
		public override string Street { get; set; }

		[MaxLength(256)]
		public override string Phone { get; set; }
		[MaxLength(256)]
		public override string Email { get; set; }

		public override double Latitude { get; set; }
		public override double Longitude { get; set; }

		public override string ToString ()
		{
			return FirstName + " " + SecondName + " " + ThirdName;
		}

	}
}

