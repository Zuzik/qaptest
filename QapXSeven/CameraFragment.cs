﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Hardware;
using Android.Content.PM;

namespace QapXSeven
{
	public class CameraFragment : Fragment, ISurfaceHolderCallback, 
	Camera.IAutoFocusCallback, Camera.IShutterCallback, Camera.IPictureCallback
	{
		static string TAG = "CameraFragment";

		Camera mCamera;
		SurfaceView mSurfaceView;
		View mProgressContainer;

		bool noCamera;
		bool autofocused=false;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			PackageManager pm = Activity.PackageManager;
			if (!pm.HasSystemFeature (PackageManager.FeatureCamera) && !pm.HasSystemFeature (PackageManager.FeatureCameraFront)) {
				noCamera = true;
			} else {
				noCamera = false;
			}
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			if (noCamera)
				return inflater.Inflate (Resource.Layout.layoutNoCamera, container, false);

			View v = inflater.Inflate (Resource.Layout.fragmentCamera, container, false);
			Button takePictureButton = v.FindViewById<Button> (Resource.Id.crime_camera_takePictureButton);
			takePictureButton.Click+= (object sender, EventArgs e) => {
				//1
				//Activity.Finish();

				//2
				if(!autofocused) mCamera.AutoFocus(this);
				else mCamera.CancelAutoFocus();
				autofocused=!autofocused;

				//3
				/*
				if(mCamera!=null){
					mCamera.TakePicture(this, null, this);
				}
				*/
			};

			mSurfaceView = v.FindViewById<SurfaceView> (Resource.Id.crime_camera_surfaceView);
			ISurfaceHolder holder = mSurfaceView.Holder;
			holder.SetType (SurfaceType.PushBuffers);
			holder.AddCallback (this);

			mProgressContainer = v.FindViewById (Resource.Id.crime_camera_progressContainer);
			mProgressContainer.Visibility = ViewStates.Invisible;

			return v;
		}

		public override void OnResume ()
		{
			base.OnResume ();

			if (noCamera)
				return;

			if (Build.VERSION.SdkInt >= BuildVersionCodes.Gingerbread) {
				mCamera = Camera.Open (0);
			} else {
				mCamera = Camera.Open ();
			}
		}

		public override void OnPause ()
		{
			base.OnPause ();

			if (mCamera != null) {
				mCamera.Release ();
				mCamera = null;
			}
		}



		void ISurfaceHolderCallback.SurfaceChanged (ISurfaceHolder holder, Android.Graphics.Format format, int width, int height)
		{
			if (mCamera == null)
				return;

			Camera.Parameters parameters = mCamera.GetParameters ();
			Camera.Size s = getBestSupportedSize(parameters.SupportedPreviewSizes, width, height);
			parameters.SetPreviewSize (s.Width, s.Height);
			s = getBestSupportedSize (parameters.SupportedPictureSizes, width, height);
			parameters.SetPictureSize (s.Width, s.Height);
			mCamera.SetParameters (parameters);
			try {
				mCamera.StartPreview();
			} catch (Exception ex) {
				Log.Error (TAG, "Could not start preview");
				mCamera.Release ();
				mCamera = null;
			}

		}
		void ISurfaceHolderCallback.SurfaceCreated (ISurfaceHolder holder)
		{
			if (noCamera)
				return;

			try {
				if(mCamera!=null){
					mCamera.SetPreviewDisplay(holder);
				}
			} catch (Exception ex) {
				Log.Error (TAG, "Error setting up preview display");
			}
		}
		void ISurfaceHolderCallback.SurfaceDestroyed (ISurfaceHolder holder)
		{
			if (noCamera)
				return;

			if (mCamera != null) {
				mCamera.StopPreview ();
			}
		}

		Camera.Size getBestSupportedSize(IList<Camera.Size> sizes, int width, int height){
			Camera.Size bestSize = sizes [0];
			int largestArea = bestSize.Width * bestSize.Height;
			foreach (var s in sizes) {
				int area = s.Width * s.Height;
				if (area > largestArea) {
					bestSize = s;
					largestArea = area;
				}
			}
			return bestSize;
		}

		void Camera.IAutoFocusCallback.OnAutoFocus (bool success, Camera camera)
		{
		}

		void Camera.IShutterCallback.OnShutter ()
		{
		}

		void Camera.IPictureCallback.OnPictureTaken (byte[] data, Camera camera)
		{
		}

	}
}

