﻿using System;
using System.Collections.Generic;
using SQLite;

namespace Zuzik
{
	public class ContactDatabase: Zuzik.AbsContactsDataBase
	{
		#region Singleton

		private static volatile AbsContactsDataBase instance;
		private static object syncRoot = new object();

		public static AbsContactsDataBase Instance {
			get {
				if (instance == null) {
					lock (syncRoot) {
						if (instance == null) {
							instance = new ContactDatabase ();
						}
					}
				}
				return instance;
			}
		}

		#endregion

		SQLiteConnection db;

		private ContactDatabase(){
		}

		#region implemented abstract members of AbsContactsDataBase

		protected override void CreateDataBase ()
		{
			db = new SQLiteConnection(DBFilePath);
			db.CreateTable<Contact> ();
		}

		protected override void CloseDataBase ()
		{
			if (db != null)
				db.Close ();
		}

		public override int SaveContact (AbsContact contact)
		{
			if (contact.ID == 0) {
				return db.Insert (contact);
			} else {
				return db.Update (contact);
			}
		}

		public override AbsContact GetContact (int primaryKey)
		{
			return db.Get<Contact> (primaryKey);
		}

		public override List<AbsContact> GetContacts ()
		{
			return new List<AbsContact> ((from s in db.Table<Contact> ()
			                              select s));
		}

		public override void DeleteContact (int primaryKey)
		{
			db.Delete<Contact> (primaryKey);
		}

		public override void DeleteAllContacts ()
		{
			db.DeleteAll<Contact> ();
		}

		protected override string DBFileName {
			get {
				return "absContactDatabaseDemo.db3";
			}
		}

		protected override string DBFilePath {
			get {
				return System.IO.Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), DBFileName);
			}
		}

		#endregion
	}
}

