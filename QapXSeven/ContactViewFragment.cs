﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace QapXSeven
{
	public class ContactViewFragment : Fragment
	{
		private static string EXTRA_CONTACT_ID = "ContactViewFragment.EXTRA_CONTACT_ID";

		Zuzik.IStartNewFragment containerActivity;

		Zuzik.AbsContact contact;

		public static ContactViewFragment GetInstance(int studentId){
			Bundle args = new Bundle ();
			args.PutInt (EXTRA_CONTACT_ID, studentId);
			ContactViewFragment fragment = new ContactViewFragment ();
			fragment.Arguments = args;
			return fragment;
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			RetainInstance = true;
			SetHasOptionsMenu (true);

			/*
			if (Arguments != null) {
				int contactId = Arguments.GetInt (EXTRA_CONTACT_ID, 0);
				contact = Zuzik.ContactDatabase.Instance.GetContact (contactId);
			} else {
				contact = new Zuzik.Contact ();
			}
			*/
		}
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			if (Arguments != null) {
				int contactId = Arguments.GetInt (EXTRA_CONTACT_ID, 0);
				contact = Zuzik.ContactDatabase.Instance.GetContact (contactId);
			} else {
				contact = new Zuzik.Contact ();
			}

			View v = inflater.Inflate (Resource.Layout.fragment_student_view, container, false);
			((TextView)v.FindViewById (Resource.Id.first_name)).Text = contact.FirstName;
			((TextView)v.FindViewById (Resource.Id.second_name)).Text = contact.SecondName;
			((TextView)v.FindViewById (Resource.Id.third_name)).Text = contact.ThirdName;
			((TextView)v.FindViewById (Resource.Id.birthday)).Text = contact.Birthday.ToShortDateString();
			((TextView)v.FindViewById (Resource.Id.country)).Text = contact.Country;
			((TextView)v.FindViewById (Resource.Id.city)).Text = contact.City;
			((TextView)v.FindViewById (Resource.Id.street)).Text = contact.Street;
			((TextView)v.FindViewById (Resource.Id.phone)).Text = contact.Phone;
			((TextView)v.FindViewById (Resource.Id.email)).Text = contact.Email;
			return v;
		}

		public override void OnAttach (Activity activity)
		{
			base.OnAttach (activity);
			containerActivity = (Zuzik.IStartNewFragment)Activity;
		}

		public override void OnDetach ()
		{
			base.OnDetach ();
			containerActivity = null;
		}

		public override void OnCreateOptionsMenu (IMenu menu, MenuInflater inflater)
		{
			inflater.Inflate (Resource.Menu.menu_student_view, menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.menu_item_remove_contact:
				Zuzik.ContactDatabase.Instance.DeleteContact (contact.ID);
				Activity.OnBackPressed ();
				return true;
			case Resource.Id.menu_item_change_contact:
				Fragment fragment = ContactChangeFragment.GetInstance (contact.ID);
				containerActivity.StartNewFragment (fragment);
				return true;
			default:
				return false;
			}
		}

	}
}

