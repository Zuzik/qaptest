﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using ZXing;
using ZXing.Mobile;

namespace QapXSeven
{
	public class ZXingFragment : Fragment
	{
		MobileBarcodeScanner scanner;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			scanner = new MobileBarcodeScanner(Activity);
			scanner.UseCustomOverlay = false;
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View v = inflater.Inflate (Resource.Layout.fragmentZXing, container, false);

			v.FindViewById<Button>(Resource.Id.zxingButton).Click+= async delegate {
				var result = await scanner.Scan();
				HandleScanResult(result);
			};

			return v;
		}

		void HandleScanResult (ZXing.Result result)
		{
			string msg = "";
			if (result != null && !string.IsNullOrEmpty(result.Text))
				msg = "Found code: " + result.Text;
			else
				msg = "Scanning Canceled!";

			Activity.RunOnUiThread(() => Toast.MakeText(Activity, msg, ToastLength.Long).Show());
		}

	}
}

