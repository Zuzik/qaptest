﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Zuzik
{
	public abstract class TabsActionBar: Activity, ActionBar.ITabListener
	{
		private static string EXTRA_SELECTED_NAVIGATION_INDEX = "selectedNavigationIndex";

		//protected abstract int GetLayoutId ();
		protected abstract int GetFragmentContainerId ();
		/// <summary>
		/// In this code, using function AddTab, need to create actionbar's tabs
		/// </summary>
		protected abstract void CreateTabs ();

		private static Dictionary<string, Stack<String>> backStacks;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			//SetContentView (GetLayoutId());

			ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;

			if (bundle == null) {
				backStacks = new Dictionary<string, Stack<string>> ();
			}

			CreateTabs ();
		}

		protected void AddTab(string tabTag, ActionBar.Tab tab){
			if (!backStacks.ContainsKey (tabTag)) {
				backStacks.Add (tabTag, new Stack<string> ());
			}
			tab.SetTag (tabTag).SetTabListener (this);
			ActionBar.AddTab (tab);
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			ActionBar.Tab tab = ActionBar.SelectedTab;
			Stack<String> backStack = backStacks [tab.Tag.ToString ()];
			if (backStack.Count != 0) {
				String tag = backStack.Peek ();
				Fragment fragment = FragmentManager.FindFragmentByTag (tag);
				if (fragment.IsDetached) {
					FragmentTransaction ft = FragmentManager.BeginTransaction ();
					ft.Attach (fragment);
					ft.Commit ();
				}
			}
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			ActionBar.Tab tab = ActionBar.SelectedTab;
			Stack<String> backStack = backStacks [tab.Tag.ToString ()];
			if (backStack.Count != 0) {
				String tag = backStack.Peek ();
				FragmentTransaction ft = FragmentManager.BeginTransaction ();
				Fragment fragment = FragmentManager.FindFragmentByTag (tag);
				ft.Detach (fragment);
				ft.Commit ();
			}
		}

		protected override void OnRestoreInstanceState (Bundle savedInstanceState)
		{
			base.OnRestoreInstanceState (savedInstanceState);
			int saved = savedInstanceState.GetInt (EXTRA_SELECTED_NAVIGATION_INDEX, 0);
			if (saved != ActionBar.SelectedNavigationIndex) {
				ActionBar.SetSelectedNavigationItem (saved);
			}
		}

		protected override void OnSaveInstanceState (Bundle outState)
		{
			base.OnSaveInstanceState (outState);
			outState.PutInt (EXTRA_SELECTED_NAVIGATION_INDEX, ActionBar.SelectedNavigationIndex);
		}

		public override void OnBackPressed ()
		{
			ActionBar.Tab tab = ActionBar.SelectedTab;
			Stack<String> backStack = backStacks [tab.Tag.ToString ()];
			String tag = backStack.Pop ();
			if (backStack.Count == 0) {
				base.OnBackPressed ();
			} else {
				FragmentTransaction ft = FragmentManager.BeginTransaction ();
				Fragment fragment = FragmentManager.FindFragmentByTag (tag);
				ft.Remove (fragment);
				ShowFragment (backStack, ft);
				ft.Commit ();
			}
		}

		public void OnTabUnselected(ActionBar.Tab tab, FragmentTransaction ft){
			Stack<String> backStack = backStacks [tab.Tag.ToString ()];
			String tag = backStack.Peek ();
			Fragment fragment = FragmentManager.FindFragmentByTag (tag);
			ft.Detach (fragment);
		}

		public void OnTabReselected(ActionBar.Tab tab, FragmentTransaction ft){
			Stack<String> backStack = backStacks [tab.Tag.ToString ()];
			while (backStack.Count > 1) {
				string tag = backStack.Pop ();
				Fragment fragment = FragmentManager.FindFragmentByTag (tag);
				ft.Remove (fragment);
			}
			ShowFragment (backStack, ft);
		}

		public abstract Fragment GetFragmentByTabTag(string tabTag);

		public void OnTabSelected(ActionBar.Tab tab, FragmentTransaction ft){
			Stack<String> backStack = backStacks [tab.Tag.ToString ()];
			if (backStack.Count == 0) {
				Fragment fragment = GetFragmentByTabTag (tab.Tag.ToString ());
				AddFragment (fragment, backStack, ft);
			} else {
				ShowFragment (backStack, ft);
			}
		}

		public void AddFragment(Fragment fragment){
			ActionBar.Tab tab = ActionBar.SelectedTab;
			Stack<String> backStack = backStacks [tab.Tag.ToString ()];
			FragmentTransaction ft = FragmentManager.BeginTransaction ();
			string tag = backStack.Peek ();
			Fragment top = FragmentManager.FindFragmentByTag (tag);
			ft.Detach(top);
			AddFragment (fragment, backStack, ft);
			ft.Commit ();
		}

		private void AddFragment(Fragment fragment, Stack<String> backStack, FragmentTransaction ft)
		{
			String tag = System.Guid.NewGuid ().ToString ();
			ft.Add (GetFragmentContainerId(), fragment, tag);
			backStack.Push(tag);
		}

		private void ShowFragment(Stack<String> backStack, FragmentTransaction ft)
		{
			string tag = backStack.Peek();
			Fragment fragment = FragmentManager.FindFragmentByTag(tag);
			ft.Attach(fragment);		
		}

	}
}

