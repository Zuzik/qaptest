﻿using System;
using System.Collections.Generic;

namespace Zuzik
{
	public abstract class AbsContactsDataBase
	{

		protected abstract string DBFileName{ get; }
		protected abstract string DBFilePath{ get; }

		protected abstract void CreateDataBase();
		protected abstract void CloseDataBase();

		public abstract int SaveContact(AbsContact contact);
		public abstract AbsContact GetContact(int primaryKey);
		public abstract List<AbsContact> GetContacts();
		public abstract void DeleteContact(int primaryKey);
		public abstract void DeleteAllContacts();

		protected AbsContactsDataBase(){
			CreateDataBase ();
		}

		~AbsContactsDataBase(){
			CloseDataBase();
		}
	}

}

