﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace QapXSeven
{
	[Activity (Label = "QapXSeven", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Zuzik.TabsActionBar, Zuzik.IStartNewFragment
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);
		}

		#region IStartNewFragment implementation

		void Zuzik.IStartNewFragment.StartNewFragment (Fragment fragment)
		{
			this.AddFragment (fragment);
		}

		#endregion

		#region implemented abstract members of TabsActionBar
		/*
		protected override int GetLayoutId ()
		{

		}
		*/

		private string TAG_TAB_LIST_CONTACTS = "qaoxseven.tag_tab_list_students";
		private string TAG_TAB_CAMERA = "qaoxseven.tag_tab_camera";
		private string TAG_TAB_ZXING = "qaoxseven.tag_tab_zxing";
		private string TAG_TAB_EMPTY_TAB = "qaoxseven.tag_tab_empty_tab";

		protected override int GetFragmentContainerId ()
		{
			return Android.Resource.Id.Content;
		}

		protected override void CreateTabs ()
		{
			AddTab (TAG_TAB_LIST_CONTACTS, ActionBar.NewTab ().SetText (Resource.String.title_tab_contact_list));
			AddTab (TAG_TAB_CAMERA, ActionBar.NewTab ().SetText (Resource.String.title_tab_camera));
			AddTab (TAG_TAB_ZXING, ActionBar.NewTab ().SetText (Resource.String.title_tab_zxing));
			AddTab (TAG_TAB_EMPTY_TAB, ActionBar.NewTab ().SetText (Resource.String.title_tab_empty_tab));
		}

		public override Fragment GetFragmentByTabTag (string tabTag)
		{
			if (tabTag.Equals (TAG_TAB_LIST_CONTACTS))
				return new ContactListFragment ();
			else if(tabTag.Equals (TAG_TAB_CAMERA))
				return new CameraFragment ();
			else if(tabTag.Equals (TAG_TAB_ZXING))
				return new ZXingFragment ();
			else if(tabTag.Equals (TAG_TAB_EMPTY_TAB))
				return new EmptyFragment ();
			else
				throw new Exception ("Unknown tag of tab");
		}

		#endregion
	}
}


