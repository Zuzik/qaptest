/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package qapxseven.qapxseven;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int background_activated=0x7f020000;
        public static final int ic_action_edit=0x7f020001;
        public static final int ic_action_new=0x7f020002;
        public static final int ic_action_remove=0x7f020003;
        public static final int ic_action_save=0x7f020004;
        public static final int ic_action_select_all=0x7f020005;
        public static final int icon=0x7f020006;
        public static final int monoandroidsplash=0x7f020007;
    }
    public static final class id {
        public static final int birthday=0x7f070004;
        public static final int city=0x7f070006;
        public static final int contact_name=0x7f07000e;
        public static final int contentFrame=0x7f07000f;
        public static final int country=0x7f070005;
        public static final int crime_camera_progressContainer=0x7f07000c;
        public static final int crime_camera_surfaceView=0x7f07000a;
        public static final int crime_camera_takePictureButton=0x7f07000b;
        public static final int dialog_date_datepicker=0x7f070000;
        public static final int email=0x7f070009;
        public static final int first_name=0x7f070001;
        public static final int menu_item_add_new_contact=0x7f070014;
        public static final int menu_item_change_contact=0x7f070015;
        public static final int menu_item_remove_contact=0x7f070011;
        public static final int menu_item_save_contact=0x7f070012;
        public static final int menu_item_select_all_contact=0x7f070010;
        public static final int phone=0x7f070008;
        public static final int search=0x7f070013;
        public static final int second_name=0x7f070002;
        public static final int street=0x7f070007;
        public static final int third_name=0x7f070003;
        public static final int zxingButton=0x7f07000d;
    }
    public static final class layout {
        public static final int fragment_date_picker=0x7f030000;
        public static final int fragment_student_change=0x7f030001;
        public static final int fragment_student_list=0x7f030002;
        public static final int fragment_student_view=0x7f030003;
        public static final int fragmentcamera=0x7f030004;
        public static final int fragmentzxing=0x7f030005;
        public static final int layoutnocamera=0x7f030006;
        public static final int list_item_student=0x7f030007;
        public static final int main=0x7f030008;
        public static final int zxingscanneractivitylayout=0x7f030009;
        public static final int zxingscannerfragmentlayout=0x7f03000a;
    }
    public static final class menu {
        public static final int context_menu_student_list=0x7f060000;
        public static final int menu_student_change=0x7f060001;
        public static final int menu_student_list=0x7f060002;
        public static final int menu_student_view=0x7f060003;
    }
    public static final class string {
        public static final int address=0x7f040010;
        public static final int app_name=0x7f040000;
        public static final int birthday=0x7f04000f;
        public static final int city=0x7f040012;
        public static final int contact=0x7f04000b;
        public static final int contacts=0x7f040014;
        public static final int country=0x7f040011;
        public static final int email=0x7f040016;
        public static final int empty_contact_list=0x7f040001;
        public static final int first_name=0x7f04000c;
        public static final int incorrect_city=0x7f04001b;
        public static final int incorrect_country=0x7f04001a;
        public static final int incorrect_first_name=0x7f040017;
        public static final int incorrect_second_name=0x7f040018;
        public static final int incorrect_street=0x7f04001c;
        public static final int incorrect_third_name=0x7f040019;
        public static final int menu_item_add_new_contact=0x7f040008;
        public static final int menu_item_change_contact=0x7f040009;
        public static final int menu_item_remove_contact=0x7f040007;
        public static final int menu_item_save_contact=0x7f04000a;
        public static final int menu_item_select_all_contact=0x7f040006;
        public static final int noCamera=0x7f04001e;
        public static final int phone=0x7f040015;
        public static final int second_name=0x7f04000d;
        public static final int street=0x7f040013;
        public static final int take=0x7f04001d;
        public static final int third_name=0x7f04000e;
        public static final int title_tab_camera=0x7f040003;
        public static final int title_tab_contact_list=0x7f040002;
        public static final int title_tab_empty_tab=0x7f040005;
        public static final int title_tab_zxing=0x7f040004;
    }
    public static final class style {
        public static final int Mono_Android_Theme_Splash=0x7f050000;
    }
}
