package qapxseven;


public class Student
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_toString:()Ljava/lang/String;:GetToStringHandler\n" +
			"";
		mono.android.Runtime.register ("QapXSeven.Student, QapXSeven, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", Student.class, __md_methods);
	}


	public Student () throws java.lang.Throwable
	{
		super ();
		if (getClass () == Student.class)
			mono.android.TypeManager.Activate ("QapXSeven.Student, QapXSeven, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public Student (int p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == Student.class)
			mono.android.TypeManager.Activate ("QapXSeven.Student, QapXSeven, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0 });
	}


	public java.lang.String toString ()
	{
		return n_toString ();
	}

	private native java.lang.String n_toString ();

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
