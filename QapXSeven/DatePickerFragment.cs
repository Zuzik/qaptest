﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace QapXSeven
{
	public class DatePickerFragment : DialogFragment, DatePicker.IOnDateChangedListener
	{
		public static string EXTRA_YEAR = "extra_year";
		public static string EXTRA_MONTH = "extra_month";
		public static string EXTRA_DAY = "extra_day";

		int year;
		int month;
		int day;

		public static DatePickerFragment NewInstance(DateTime date){
			Bundle args = new Bundle ();
			args.PutInt (EXTRA_YEAR, date.Year);
			args.PutInt (EXTRA_MONTH, date.Month-1);
			args.PutInt (EXTRA_DAY, date.Day);
			DatePickerFragment fragment = new DatePickerFragment ();
			fragment.Arguments = args;
			return fragment;
		}

		public static DatePickerFragment NewInstance(int year, int month, int day){
			Bundle args = new Bundle ();
			args.PutInt (EXTRA_YEAR, year);
			args.PutInt (EXTRA_MONTH, month);
			args.PutInt (EXTRA_DAY, day);
			DatePickerFragment fragment = new DatePickerFragment ();
			fragment.Arguments = args;
			return fragment;
		}

		private void sendResult(Result resultCode) {
			if (TargetFragment == null)
				return;
			Intent i = new Intent();
			i.PutExtra(EXTRA_YEAR, year);
			i.PutExtra(EXTRA_MONTH, month+1);
			i.PutExtra(EXTRA_DAY, day);
			TargetFragment.OnActivityResult (TargetRequestCode, resultCode, i);
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override Dialog OnCreateDialog (Bundle savedInstanceState)
		{
			year = Arguments.GetInt (EXTRA_YEAR);
			month = Arguments.GetInt (EXTRA_MONTH);
			day = Arguments.GetInt (EXTRA_DAY);

			View v = Activity.LayoutInflater.Inflate (Resource.Layout.fragment_date_picker, null);

			DatePicker datePicker = v.FindViewById<DatePicker> (Resource.Id.dialog_date_datepicker);
			datePicker.Init (year, month, day, this);

			AlertDialog.Builder a = new AlertDialog.Builder (Activity);
			a.SetView (v);
			a.SetTitle (Resource.String.birthday);
			a.SetPositiveButton (Android.Resource.String.Ok, OnOkClick);
			a.SetNegativeButton (Android.Resource.String.Cancel, OnCancelClick);

			return a.Create ();
		}

		void OnOkClick(object sender, DialogClickEventArgs e){
			sendResult (Result.Ok);
		}

		void OnCancelClick(object sender, DialogClickEventArgs e){
			sendResult (Result.Canceled);
		}

		public void OnDateChanged(DatePicker view, int year, int month, int day){
			this.year = year;
			this.month = month;
			this.day = day;
			Arguments.PutInt (EXTRA_YEAR, this.year);
			Arguments.PutInt (EXTRA_MONTH, this.month);
			Arguments.PutInt (EXTRA_DAY, this.day);
		}

	}
}

